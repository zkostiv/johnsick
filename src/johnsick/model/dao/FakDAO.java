/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package johnsick.model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import johnsick.model.Fak;
import johnsick.model.db.ConnectionFactory;
import static johnsick.model.db.ConnectionFactory.connection;
import johnsick.model.db.DbUtil;

/**
 *
 * @author mk
 */
public class FakDAO {

    private PreparedStatement ps;

    private Fak getFakFromResultset(ResultSet rs) {
        Fak fak = null;
        try {
            fak = new Fak(rs.getInt("id"), rs.getString("name"));
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fak;
    }
    
    /**
     * Gets all Fak from database
     * @return 
     */
    public List<Fak> GetAll() {

        ResultSet rs = null;
        List<Fak> result = new ArrayList<>();
        try {
            connection = ConnectionFactory.getConnection();
            ps = connection.prepareStatement("SELECT * FROM fak");
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(getFakFromResultset(rs));
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
        return result;
    }

    /**
     * Insert or Update Fak into table
     *
     * @param fakultatyv
     * @return
     */
    public Fak saveFak(Fak fakultatyv) {
        try {
            //connection = ConnectionFactory.getConnection();
            if (fakultatyv.getId() == null || fakultatyv.getId() == -1) {
                ps = connection.prepareStatement("INSERT INTO fak(name) VALUES(?)");
                ps.setString(1, fakultatyv.getName());
                ps.executeUpdate();
                ps = connection.prepareStatement("SELECT last_insert_rowid();");
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    fakultatyv.setId(rs.getInt(1));
                }

            } else {
                ps = connection.prepareStatement("UPDATE fak SET `name`=?  WHERE rowid=?");
                ps.setString(1, fakultatyv.getName());
                ps.setInt(2, fakultatyv.getId());
                ps.executeUpdate();
            }
            DbUtil.close(ps);
        } catch (SQLException ex) {
            Logger.getLogger(Fak.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fakultatyv;
    }
}
