/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package johnsick.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import johnsick.model.Student;
import johnsick.model.db.ConnectionFactory;
import static johnsick.model.db.ConnectionFactory.connection;
import johnsick.model.db.DbUtil;

/**
 *
 * @author mk
 */
public class StudentDAO {
    private PreparedStatement ps;

    public StudentDAO() {
    }

    private Student getStudentFromResultset(ResultSet rs) {
        Student student = new Student();
        try {
            student.setId(rs.getLong("id"));
            student.setFirstName(rs.getString("firstname"));
            student.setMiddleName(rs.getString("middlename"));
            student.setLastName(rs.getString("lastname"));
            student.setAddress(rs.getString("address"));
            student.setPhone(rs.getString("phone"));
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return student;
    }
    
    /**
     * Gets all students from database
     * @return 
     */
    public List<Student> GetAll() {
        ResultSet rs = null;
        List<Student> result = new ArrayList<>();
        try {
          //  connection = ConnectionFactory.getConnection();
            ps = connection.prepareStatement("SELECT * FROM students");
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(getStudentFromResultset(rs));
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
        return result;
    }
}
