/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package johnsick.model;

import java.sql.Date;
import java.util.List;

/**
 *
 * @author mk
 */
public class Fak {
    private Integer id;
    private String name;
    private Integer lectionHoursCount;
    private Integer practicHoursCount;
    private Integer laborHoursCount;
    private Date startDate;
    private Date endDate;
    private List<Student> subscribedStudents;

    public Fak(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLectionHoursCount() {
        return lectionHoursCount;
    }

    public void setLectionHoursCount(Integer lectionHoursCount) {
        this.lectionHoursCount = lectionHoursCount;
    }

    public Integer getPracticHoursCount() {
        return practicHoursCount;
    }

    public void setPracticHoursCount(Integer practicHoursCount) {
        this.practicHoursCount = practicHoursCount;
    }

    public Integer getLaborHoursCount() {
        return laborHoursCount;
    }

    public void setLaborHoursCount(Integer laborHoursCount) {
        this.laborHoursCount = laborHoursCount;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<Student> getSubscribedStudents() {
        return subscribedStudents;
    }

    public void setSubscribedStudents(List<Student> subscribedStudents) {
        this.subscribedStudents = subscribedStudents;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
}
